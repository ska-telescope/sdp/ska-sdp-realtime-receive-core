import copy
import datetime
import shutil
import subprocess
import tempfile
import time
from pathlib import Path
from typing import Tuple

import numpy as np
import pytest
from astropy.coordinates import Angle
from casacore import tables
from pytest_benchmark.fixture import BenchmarkFixture

from realtime.receive.core import (
    Antenna,
    antenna_utils,
    baseline_utils,
    msutils,
    scan_utils,
    time_utils,
)
from realtime.receive.core.baselines import Baselines
from realtime.receive.core.common import untar
from realtime.receive.core.ms_asserter import MSAsserter
from realtime.receive.core.msutils import (
    MeasurementSet,
    Mode,
    TensorRef,
    vis_mjd_epoch,
    vis_reader,
)
from realtime.receive.core.pointing import Pointing
from realtime.receive.core.scan import FrequencyType, PhaseDirection, Scan
from realtime.receive.core.time_utils import unix_as_astropy, unix_to_mjd
from realtime.receive.core.uvw_engine import UVWEngine

PLASMA_SOCKET = "/tmp/plasma"

# pylint: disable=missing-function-docstring


def plasmastman_installed():
    if shutil.which("ldconfig") is not None:
        with subprocess.Popen("ldconfig -p".split(), stdout=subprocess.PIPE) as process:
            libs = process.communicate()[0].decode("utf-8")
            return libs.find("plasmastman.so") > 0
    else:
        return None


def plasma_installed():
    return shutil.which("plasma_store") is not None


@pytest.fixture
def temp_ms(tmpdir_factory) -> Path:
    test_file = "test.ms"
    return tmpdir_factory.mktemp("testdata").join(test_file)


@pytest.fixture
def antennas() -> Tuple[Antenna]:
    all_antennas = antenna_utils.load_antennas("tests/data/low-layout.json")
    return tuple(all_antennas[1:5])


@pytest.fixture
def plasma_store():
    if plasma_installed():
        with subprocess.Popen(["plasma_store", "-m", "10000000", "-s", PLASMA_SOCKET]) as proc:
            yield proc
            proc.terminate()
            proc.wait()
    else:
        yield None


@pytest.mark.parametrize(
    "sb_file",
    [
        "tests/data/assign_resources_0_4.json",
        "tests/data/assign_resources_1_0.json",
    ],
)
@pytest.mark.parametrize(
    "plasmastman",
    [
        pytest.param(
            True,
            marks=pytest.mark.skipif(
                not plasmastman_installed(), reason="plasmastman not installed"
            ),
        ),
        pytest.param(False),
    ],
)
# pylint: disable=unused-argument
def test_write_single_payload(
    temp_ms: Path,
    sb_file: str,
    antennas: Tuple[Antenna],
    plasmastman: bool,
    plasma_store,
):
    num_baselines = baseline_utils.baselines(antennas, True)
    scan_types = scan_utils.parse_scantypes_from_assignres(sb_file)
    scan = Scan(0, scan_types[0])
    beam = scan.scan_type.beams[0]
    sw = beam.channels.spectral_windows[0]

    scan_id = 1
    timestamp = time.time()

    if not plasmastman:
        visibilities = np.zeros(
            [
                num_baselines,
                scan.scan_type.num_channels,
                scan.scan_type.num_pols,
            ]
        )
    else:
        # pylint: disable-next=import-error,import-outside-toplevel
        import pyarrow

        # pylint: disable-next=import-error,import-outside-toplevel
        from pyarrow import plasma

        def create_tensor_ref(array: np.ndarray) -> TensorRef:
            # Alternative helper utilities available in ska_sdp_dal
            # import ska_sdp_dal
            # vis = ska_sdp_dal.Store(PLASMA_SOCKET).put_new_tensor(np.zeros([]))
            # return TensorRef(vis.oid.binary(), vis.get())

            client: plasma.PlasmaClient = plasma.connect(PLASMA_SOCKET)
            tensor: np.ndarray = pyarrow.Tensor.from_numpy(array)
            tensor_size = pyarrow.ipc.get_tensor_size(tensor)
            oid = plasma.ObjectID(bytes(20))
            out_buf: plasma.PlasmaBuffer = client.create(oid, tensor_size)
            writer = pyarrow.FixedSizeBufferWriter(out_buf)
            pyarrow.ipc.write_tensor(tensor, writer)
            client.seal(oid)
            return TensorRef(oid.binary(), out_buf)

        visibilities = create_tensor_ref(
            np.zeros(
                [
                    num_baselines,
                    scan.scan_type.num_channels,
                    scan.scan_type.num_pols,
                    2,
                ],
                np.float32,
            )
        )

    uvw = UVWEngine(antennas).get_uvw(unix_as_astropy(timestamp), beam.field.phase_dir)
    with msutils.MSWriter(
        str(temp_ms),
        scan,
        antennas=antennas,
        plasma_socket=PLASMA_SOCKET if plasmastman else None,
    ) as measurement_set_writer:
        measurement_set_writer.write_data_row(
            scan_id=scan_id,
            beam=beam,
            sw=sw,
            payload_seq_no=0,
            mjd_time=unix_to_mjd(timestamp),
            interval=0,
            exposure=0,
            uvw=uvw,
            vis=visibilities,
            flag=None,
            weight=None,
            sigma=None,
        )

    with msutils.MeasurementSet.open(str(temp_ms)) as ms:
        assert ms.num_rows == num_baselines
        assert (ms.read_column("SCAN_NUMBER") == 1).all()
        np.testing.assert_array_equal(ms.read_cell("DATA", 0), np.zeros([4, 4]))
        np.testing.assert_array_equal(ms.read_cell("FLAG", 0), np.zeros([4, 4]))
        np.testing.assert_array_equal(ms.read_cell("UVW", 0), np.zeros([3]))
        np.testing.assert_array_equal(ms.read_cell("WEIGHT", 0), np.ones([4]))
        np.testing.assert_array_equal(ms.read_cell("SIGMA", 0), np.ones([4]))


@pytest.mark.parametrize(
    "gen_flags,gen_weights,gen_sigmas",
    [
        (False, False, False),
        (True, True, True),
    ],
)
@pytest.mark.parametrize(
    "assign_resources_file",
    [
        "tests/data/adr54-assign-resources-example.json",
    ],
)
def test_write_single_beam_payloads(
    temp_ms: Path, assign_resources_file: str, antennas, gen_flags, gen_weights, gen_sigmas
):
    # pylint: disable=protected-access
    SCAN_NUMBER = 0
    SCAN_TYPE_NUMBER = 0
    BEAM_NUMBER = 0
    num_baselines = baseline_utils.baselines(antennas, True)
    scan_types = scan_utils.parse_scantypes_from_assignres(assign_resources_file)
    scan = Scan(SCAN_NUMBER, scan_types[SCAN_TYPE_NUMBER])
    uvw_engine = UVWEngine(antennas)
    beam = scan.scan_type.beams[BEAM_NUMBER]
    ms_writer = msutils.MSWriter(str(temp_ms), scan, antennas)

    assert 3 == len(ms_writer.ms._model.spectral_windows)
    assert 1 == len(ms_writer.ms._model.fields)
    assert 1 == len(ms_writer.ms._model.beams)

    assert scan.scan_number == SCAN_NUMBER
    st = scan.scan_type
    payload_number = 0
    for beam in [st.beams[BEAM_NUMBER]]:
        # multiple spectral windows should be present in config
        assert len(beam.channels.spectral_windows) > 1
        for sw in beam.channels.spectral_windows:
            scan_id = scan.scan_number
            timestamp = time.time()
            visibilities = np.zeros(
                [
                    num_baselines,
                    st.num_channels,
                    st.num_pols,
                ]
            )

            flags = (
                np.ones(
                    [
                        num_baselines,
                        scan.scan_type.num_channels,
                        scan.scan_type.num_pols,
                    ],
                    dtype=np.bool_,
                )
                if gen_flags
                else None
            )  # defaults to 0

            weights = (
                np.ones(
                    [
                        num_baselines,
                        scan.scan_type.num_pols,
                    ],
                    dtype=np.float32,
                )
                * 2
                if gen_weights
                else None
            )  # defaults to 1

            sigmas = (
                np.ones(
                    [
                        num_baselines,
                        scan.scan_type.num_pols,
                    ],
                    dtype=np.float32,
                )
                * 2
                if gen_sigmas
                else None
            )  # defaults to 1

            uvw = uvw_engine.get_uvw(
                unix_as_astropy(timestamp),
                beam.field.phase_dir,
            )
            assert uvw is not None
            ms_writer.write_data_row(
                scan_id,
                beam,
                sw,
                payload_number,
                unix_to_mjd(timestamp),
                interval=0,
                exposure=0,
                uvw=uvw,
                vis=visibilities,
                flag=flags,
                weight=weights,
                sigma=sigmas,
            )

            msasserter = MSAsserter()
            msasserter.assert_ms_integrity(ms_writer.ms.name)

            maintable = tables.table(ms_writer.ms.name, readonly=True)
            assert (maintable.getcol("SCAN_NUMBER") == 0).all()
            swtable = tables.table(f"{maintable.name()}/SPECTRAL_WINDOW", readonly=True)
            assert swtable.getcell("NAME", payload_number) == sw.spectral_window_id
            assert swtable.getcell("MEAS_FREQ_REF", payload_number) == FrequencyType.TOPO.value

            main_table_row = payload_number * num_baselines
            np.testing.assert_array_equal(
                maintable.getcell("DATA", main_table_row),
                np.zeros([st.num_channels, st.num_pols]),
            )
            np.testing.assert_array_equal(
                maintable.getcell("FLAG", main_table_row),
                np.ones([st.num_channels, st.num_pols])
                if gen_flags
                else np.zeros([st.num_channels, st.num_pols]),
            )
            np.testing.assert_array_equal(
                maintable.getcell("UVW", main_table_row).shape, np.array([3])
            )
            np.testing.assert_array_equal(
                maintable.getcell("WEIGHT", main_table_row),
                np.ones([st.num_pols]) * 2 if gen_weights else np.ones([st.num_pols]),
            )
            np.testing.assert_array_equal(
                maintable.getcell("SIGMA", main_table_row),
                np.ones([st.num_pols]) * 2 if gen_sigmas else np.ones([st.num_pols]),
            )
            swtable.close()
            maintable.close()
            payload_number += 1
    ms_writer.close()

    ms = msutils.MeasurementSet(str(temp_ms), mode=msutils.Mode.READONLY)

    # check ms scan_model
    ms_scan_type = ms.calculate_scan_type()
    assert 1 == len(ms_scan_type.beams)
    assert 3 == len(ms_scan_type.beams[BEAM_NUMBER].channels.spectral_windows)
    assert 744 == ms_scan_type.num_channels
    assert 4 == ms_scan_type.num_pols

    # check expected discrepencies (these values have no respective entries
    # inside measurement set tables)
    expected_scan_type = copy.deepcopy(scan_types[0])
    expected_scan_type.scan_type_id = "0"
    expected_beam = expected_scan_type.beams[BEAM_NUMBER]
    expected_beam.beam_id = "0"
    expected_beam.function = "missing"
    expected_beam.channels.channels_id = "0"
    expected_beam.channels.spectral_windows[0].start = 0
    expected_beam.channels.spectral_windows[1].start = 0
    expected_beam.channels.spectral_windows[2].start = 0
    expected_beam.polarisations.polarisation_id = "XX,XY,YY,YX"
    original_phase_dir = expected_beam.field.phase_dir
    expected_beam.field.phase_dir = PhaseDirection(
        Angle(original_phase_dir.ra.rad, "rad"),
        Angle(original_phase_dir.dec.rad, "rad"),
        "",
    )
    expected_beam.field.pointing_fqdn = None
    assert ms_scan_type == expected_scan_type


@pytest.mark.parametrize(
    "assign_resources_file",
    [
        "tests/data/adr54-assign-resources-example.json",
    ],
)
@pytest.mark.parametrize(
    "reverse_beams,reverse_sw",
    [
        (False, False),
        (False, True),
        (True, False),
        (False, True),
    ],
)
def test_write_multiple_beam_payloads(
    temp_ms: Path,
    assign_resources_file: str,
    reverse_beams: bool,
    reverse_sw: bool,
    antennas: Tuple[Antenna],
):
    scan_types = scan_utils.parse_scantypes_from_assignres(assign_resources_file)
    num_baselines = baseline_utils.baselines(antennas, True)
    scan = Scan(1, scan_types[1])

    uvw_engine = UVWEngine(antennas)

    # pylint: disable=protected-access
    measurement_set_writer = msutils.MSWriter(str(temp_ms), scan, antennas)
    assert 2 == len(measurement_set_writer.ms._model.beams)

    st = scan.scan_type
    payload_number = 0
    beams = reversed(st.beams) if reverse_beams else st.beams
    for beam in beams:
        spectral_windows = (
            reversed(beam.channels.spectral_windows)
            if reverse_sw
            else beam.channels.spectral_windows
        )
        for sw in spectral_windows:
            timestamp = time.time()
            scan_id = scan.scan_number
            visibilities = np.zeros(
                [
                    num_baselines,
                    st.num_channels,
                    beam.polarisations.num_pols,
                ],
                dtype=np.complex128,
            )
            uvw = uvw_engine.get_uvw(
                unix_as_astropy(timestamp),
                beam.field.phase_dir,
            )
            assert uvw is not None

            mjd_time = unix_to_mjd(timestamp)
            predicted_row = measurement_set_writer._calculate_insertion_row(
                payload_number, mjd_time
            )
            # TODO: I'm not using any offset for multiple beams etc ...
            # which seemed to be implied by get_data_offset.

            measurement_set_writer.write_data_row(
                scan_id,
                beam,
                sw,
                payload_number,
                mjd_time,
                interval=1,
                exposure=1,
                uvw=uvw,
                vis=visibilities,
                flag=None,
                weight=None,
                sigma=None,
            )

            msasserter = MSAsserter()
            msasserter.assert_ms_integrity(measurement_set_writer.ms.name)

            maintable = tables.table(measurement_set_writer.ms.name, readonly=True)

            assert maintable.getcell("SCAN_NUMBER", predicted_row) == 1
            swtable = tables.table(f"{maintable.name()}/SPECTRAL_WINDOW", readonly=True)

            np.testing.assert_array_equal(
                maintable.getcell("DATA", predicted_row),
                np.zeros([st.num_channels, st.num_pols]),
            )
            np.testing.assert_array_equal(
                maintable.getcell("FLAG", predicted_row),
                np.zeros([st.num_channels, st.num_pols]),
            )
            np.testing.assert_array_equal(
                maintable.getcell("UVW", predicted_row).shape, np.array([3])
            )
            np.testing.assert_array_equal(
                maintable.getcell("WEIGHT", predicted_row),
                np.ones([st.num_pols]),
            )
            np.testing.assert_array_equal(
                maintable.getcell("SIGMA", predicted_row),
                np.ones([st.num_pols]),
            )
            swtable.close()
            maintable.close()
            payload_number += 1
    measurement_set_writer.close()

    # check ms scan_model
    ms = msutils.MeasurementSet(str(temp_ms), mode=msutils.Mode.READONLY)
    ms_scan_type = ms.calculate_scan_type()
    assert 2 == len(ms_scan_type.beams)
    assert 1 == len(ms_scan_type.beams[0].channels.spectral_windows)
    assert 1 == len(ms_scan_type.beams[1].channels.spectral_windows)
    assert 744 == ms_scan_type.num_channels
    assert 4 == ms_scan_type.num_pols

    # check expected discrepencies (these values have no
    # respective entries inside measurement set tables)
    expected_scan_type = copy.deepcopy(scan_types[1])
    expected_scan_type.scan_type_id = "0"
    for beam_id in ("0", "1"):
        beam_id_int = int(beam_id)
        expected_beam = expected_scan_type.beams[beam_id_int]
        expected_beam.beam_id = beam_id
        expected_beam.function = "missing"
        expected_beam.channels = copy.copy(expected_beam.channels)
        expected_beam.channels.channels_id = beam_id
        expected_beam.channels.spectral_windows[0].start = 0
        expected_beam.polarisations.polarisation_id = "XX,XY,YY,YX"
        original_phase_dir = expected_beam.field.phase_dir
        expected_beam.field.phase_dir = PhaseDirection(
            Angle(original_phase_dir.ra.rad, "rad"),
            Angle(original_phase_dir.dec.rad, "rad"),
            "",
        )
        expected_beam.field.pointing_fqdn = None
        if beam_id == "0" and reverse_beams is False:
            expected_beam.field.field_id = "pss_field_0"
        if beam_id == "0" and reverse_beams is True:
            expected_beam.field.field_id = "pss_field_1"

        if beam_id == "1" and reverse_beams is False:
            expected_beam.field.field_id = "pss_field_1"
        if beam_id == "1" and reverse_beams is True:
            expected_beam.field.field_id = "pss_field_0"

    assert ms_scan_type == expected_scan_type


@pytest.mark.parametrize(
    "sb_file",
    [
        "tests/data/assign_resources_0_4.json",
        "tests/data/assign_resources_1_0.json",
    ],
)
def test_set_telescope(monkeypatch, temp_ms: Path, sb_file: str, antennas: Tuple[Antenna]):
    scan_types = scan_utils.parse_scantypes_from_assignres(sb_file)
    scan = Scan(0, scan_types[0])
    monkeypatch.setenv("TELESCOPE_NAME", "SKA_Low")
    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        # antenna table created during ms writer construction
        pass
    observation = tables.table(f"{ms_writer.ms.name}/OBSERVATION", readonly=True)
    telescope_name = observation.getcol("TELESCOPE_NAME")[0]
    assert telescope_name == "SKA_Low"


@pytest.mark.parametrize(
    "sb_file",
    [
        "tests/data/assign_resources_0_4.json",
        "tests/data/assign_resources_1_0.json",
    ],
)
def test_set_observer(monkeypatch, temp_ms: Path, sb_file: str, antennas: Tuple[Antenna]):
    scan_types = scan_utils.parse_scantypes_from_assignres(sb_file)
    scan = Scan(0, scan_types[0])
    monkeypatch.setenv("USERNAME", "Arthur Morgan")
    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        # antenna table created during ms writer construction
        pass
    observation = tables.table(f"{ms_writer.ms.name}/OBSERVATION", readonly=True)
    username = observation.getcol("OBSERVER")[0]
    assert username == "Arthur Morgan"


@pytest.mark.parametrize(
    "sb_file",
    [
        "tests/data/assign_resources_0_4.json",
        "tests/data/assign_resources_1_0.json",
    ],
)
def test_write_antenna_table(temp_ms: Path, sb_file: str, antennas: Tuple[Antenna]):
    scan_types = scan_utils.parse_scantypes_from_assignres(sb_file)
    scan = Scan(0, scan_types[0])
    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        # antenna table created during ms writer construction
        pass
    ants = tables.table(f"{ms_writer.ms.name}/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    assert pos_0[0] == -2565124.7717066384
    assert pos_0[1] == 5085647.529621617
    assert pos_0[2] == -2861207.079154239

    pos_1 = ants.getcell("POSITION", 1)
    assert pos_1[0] == -2564996.841508992
    assert pos_1[1] == 5085851.958070379
    assert pos_1[2] == -2860957.8376849536

    diameter = ants.getcell("DISH_DIAMETER", 0)
    assert diameter == 38.0

    name = ants.getcell("NAME", 0)
    assert name == "C1"


@pytest.mark.parametrize(
    "ms_path, expected_mjd_epoch",
    [
        ("tests/data/low_sim.ms", 4453428720.979),
        ("tests/data/1934_SB4094_b0_t0_ch0.ms", 5010709509.713088),
    ],
)
def test_read_vis_mjd_epoch(ms_path, expected_mjd_epoch):
    untar(ms_path)
    with MeasurementSet.open(ms_path) as ms:
        assert expected_mjd_epoch == vis_mjd_epoch(ms)


@pytest.mark.asyncio
async def test_vis_reader():
    """
    Tests that the vis_reader async iterable works against different files
    """

    async def read_all(ms):
        read = 0
        async for _ in vis_reader(ms):
            read += 1
        assert read > 0

    for ms in ("tests/data/low_sim.ms", "tests/data/1934_SB4094_b0_t0_ch0.ms"):
        untar(ms)
        await read_all(ms)


def test_write_pointings(temp_ms: Path, antennas: Tuple[Antenna]):
    scan_types = scan_utils.parse_scantypes_from_assignres("tests/data/assign_resources_1_0.json")
    scan = Scan(0, scan_types[0])
    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        test_time = time.mktime(datetime.datetime(2023, 3, 29).timetuple())
        ms_writer.write_pointings(
            [
                Pointing(
                    antenna_id=1,
                    time=test_time,
                    direction=(30.0, 20.0),
                    target=(29.9, 19.9),
                    source_offset=(0.1, 0.2),
                    pointing_offset=(0.3, 0.4),
                )
            ]
        )
    points = tables.table(f"{ms_writer.ms.name}/POINTING", readonly=True)
    assert 1 == points.getcell("ANTENNA_ID", 0)
    assert unix_to_mjd(test_time) == points.getcell("TIME", 0)
    assert 0.0 == points.getcell("INTERVAL", 0)
    assert "" == points.getcell("NAME", 0)
    assert 0 == points.getcell("NUM_POLY", 0)
    assert unix_to_mjd(test_time) == points.getcell("TIME_ORIGIN", 0)
    np.testing.assert_array_equal([[30.0, 20.0]], points.getcell("DIRECTION", 0))
    np.testing.assert_array_equal([[29.9, 19.9]], points.getcell("TARGET", 0))
    np.testing.assert_array_equal([[0.3, 0.4]], points.getcell("POINTING_OFFSET", 0))
    np.testing.assert_array_equal([[0.1, 0.2]], points.getcell("SOURCE_OFFSET", 0))
    assert points.getcell("TRACKING", 0)


@pytest.mark.parametrize("names", [["", ""], ["ABC", ""], ["", "ABC"]])
def test_write_pointing_empty_names(
    temp_ms: Path,
    antennas: tuple[Antenna],
    names: list[str],
):
    scan_types = scan_utils.parse_scantypes_from_assignres("tests/data/assign_resources_1_0.json")
    scan = Scan(0, scan_types[0])
    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        ms_writer.write_pointings(
            list(
                Pointing(
                    antenna_id=1,
                    time=time.time(),
                    direction=(30.0, 20.0),
                    target=(29.9, 19.9),
                    name=name,
                )
                for name in names
            )
        )

    points = tables.table(f"{ms_writer.ms.name}/POINTING", readonly=True)
    assert len(names) == points.nrows()
    for i, name in enumerate(names):
        assert name == points.getcell("NAME", i)


# From https://confluence.skatelescope.org/display/SWSI/Array+Assemblies
AA05_DISHES = 4
AA2_DISHES = 68
AASTAR_DISHES = 144
FIVE_MIN_OB_POINTING_COUNT = 5 * 60 * 10


@pytest.mark.benchmark
@pytest.mark.parametrize("num_antennas", [AA05_DISHES, AA2_DISHES, AASTAR_DISHES])
def test_write_pointing_performance(
    benchmark: BenchmarkFixture, antennas: tuple[Antenna], num_antennas: int
):
    scan_types = scan_utils.parse_scantypes_from_assignres("tests/data/assign_resources_1_0.json")
    scan = Scan(0, scan_types[0])
    pointings = [
        Pointing(
            antenna_id=i % len(antennas),
            time=time.time(),
            direction=(30.0, 20.0),
            target=(29.9, 19.9),
            source_offset=(0.1, 0.2),
            pointing_offset=(0.3, 0.4),
        )
        for i in range(num_antennas * FIVE_MIN_OB_POINTING_COUNT)
    ]

    def do_benchmark():
        with tempfile.TemporaryDirectory(suffix=".ms") as temp_ms, msutils.MSWriter(
            str(temp_ms), scan, antennas
        ) as ms_writer:
            ms_writer.write_pointings(pointings)
            points = tables.table(f"{temp_ms}/POINTING", readonly=True)
            return points.nrows()

    nrows = benchmark(do_benchmark)
    assert nrows == len(pointings)


@pytest.mark.parametrize(
    "sb_file",
    [
        "tests/data/assign_resources_0_4.json",
        "tests/data/assign_resources_1_0.json",
    ],
)
def test_field_table(temp_ms: Path, sb_file: str, antennas: Tuple[Antenna]):
    scan_types = scan_utils.parse_scantypes_from_assignres(sb_file)
    scan = Scan(0, scan_types[0])
    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        pass
    field = tables.table(f"{ms_writer.ms.name}/FIELD", readonly=True)
    pos_0 = field.getcell("PHASE_DIR", 0)
    np.testing.assert_allclose(pos_0[0][0], 0.709823297846581, rtol=1e-6)
    np.testing.assert_allclose(pos_0[0][1], -0.00023193486504280203, rtol=1e-6)

    pos_1 = field.getcell("REFERENCE_DIR", 0)
    assert pos_1[0][0] == pos_0[0][0]
    assert pos_1[0][1] == pos_0[0][1]

    pos_1 = field.getcell("DELAY_DIR", 0)
    assert pos_1[0][0] == pos_0[0][0]
    assert pos_1[0][1] == pos_1[0][1]

    name = field.getcell("NAME", 0)
    assert name == "field_a"


@pytest.mark.parametrize("lower_triangular", (True, False))
def test_write_baselines(temp_ms: Path, antennas: Tuple[Antenna], lower_triangular: bool):
    """
    Ensure the MeasurementSet class preserves the antenna baseline ordering
    given at construction time.
    """
    scan_types = scan_utils.parse_scantypes_from_assignres("tests/data/assign_resources_1_0.json")
    scan = Scan(0, scan_types[0])
    beam = scan_types[0].beams[0]

    baselines = Baselines.generate(len(antennas), autocorr=True, lower_triangular=lower_triangular)
    visibilities = np.zeros(
        [
            len(baselines),
            beam.channels.num_channels,
            beam.polarisations.num_pols,
        ]
    )
    now = time.time()
    uvw = UVWEngine(antennas).get_uvw(
        unix_as_astropy(now),
        beam.field.phase_dir,
    )

    with msutils.MSWriter(str(temp_ms), scan, antennas, baselines) as ms_writer:
        ms_writer.write_data_row(
            scan.scan_number,
            beam,
            beam.channels.spectral_windows[0],
            0,
            time_utils.unix_to_mjd(now),
            interval=1,
            exposure=1,
            uvw=uvw,
            vis=visibilities,
            flag=None,
            weight=None,
            sigma=None,
        )

    with msutils.MeasurementSet.open(str(temp_ms)) as ms:
        a1 = ms.read_column("ANTENNA1")  # int32
        a2 = ms.read_column("ANTENNA2")  # int32
    np.testing.assert_array_equal(a1, baselines.antenna1)
    np.testing.assert_array_equal(a2, baselines.antenna2)


def test_read_scantype_from_ms():
    RTOL = 1e-15
    ms = MeasurementSet.open("data/sim-vis.ms", Mode.READONLY)
    scan_type = ms.calculate_scan_type()
    assert 4 == scan_type.num_channels
    assert 4 == scan_type.num_pols
    assert 1 == len(scan_type.beams)
    assert 1 == len(scan_type.beams[0].channels.spectral_windows)

    sw = scan_type.beams[0].channels.spectral_windows[0]
    np.testing.assert_allclose(sw.freq_min, 149875000.0, rtol=RTOL)
    np.testing.assert_allclose(sw.freq_max, 150075000.0, rtol=RTOL)
    np.testing.assert_allclose(
        sw.channel_width,
        50000.00000000001,
        rtol=1e-10,  # conversion loss converting between model to ms
    )

    assert isinstance(
        scan_type.beams[0].field.phase_dir.ra.rad,
        np.ndarray,
    )
    assert isinstance(
        scan_type.beams[0].field.phase_dir.dec.rad,
        np.ndarray,
    )
    np.testing.assert_allclose(
        np.array([[0.0], [-0.4667010419832837]]),
        np.array(
            [
                scan_type.beams[0].field.phase_dir.ra,
                scan_type.beams[0].field.phase_dir.dec,
            ]
        ),
        rtol=RTOL,
    )


@pytest.mark.parametrize(
    "ar_path, scan_type_id, expected_strides",
    [
        (
            "tests/data/adr54-assign-resources-example.json",
            "target:a",
            {"fsp_1_channels": 2, "fsp_2_channels": 1, "zoom_window_1": 1},
        ),
        (
            "tests/data/adr54-assign-resources-example.json",
            "target:pss",
            {"pulsar_fsp_channels": 1},
        ),
    ],
)
def test_write_spectral_window_stride(
    temp_ms: Path,
    antennas: Tuple[Antenna],
    ar_path: str,
    scan_type_id: str,
    expected_strides: dict[str, int],
):
    scan_type = next(
        st
        for st in scan_utils.parse_scantypes_from_assignres(ar_path)
        if st.scan_type_id == scan_type_id
    )
    scan = Scan(0, scan_type)
    beam = scan.scan_type.beams[0]

    with msutils.MSWriter(str(temp_ms), scan, antennas) as ms_writer:
        # Opening logic assumes there's some data in the main table for each spectral window
        now = time.time()
        uvw = UVWEngine(antennas).get_uvw(
            unix_as_astropy(now),
            beam.field.phase_dir,
        )

        baselines = Baselines.generate(len(antennas), autocorr=True)
        visibilities = np.zeros(
            [
                len(baselines),
                beam.channels.num_channels,
                beam.polarisations.num_pols,
            ]
        )

        for seq_no, sw in enumerate(beam.channels.spectral_windows):
            ms_writer.write_data(
                scan.scan_number,
                beam,
                sw,
                seq_no,
                time_utils.unix_to_mjd(now),
                interval=1,
                exposure=1,
                first_chan=0,
                chan_count=beam.channels.num_channels,
                uvw=uvw,
                vis=visibilities,
                flag=None,
                weight=None,
                sigma=None,
            )

    with MeasurementSet.open(str(temp_ms)) as ms:
        spectral_windows = [
            sw for b in ms.calculate_scan_type().beams for sw in b.channels.spectral_windows
        ]
        assert len(spectral_windows) == len(expected_strides)
        for spectral_window in spectral_windows:
            assert expected_strides[spectral_window.spectral_window_id] == spectral_window.stride
