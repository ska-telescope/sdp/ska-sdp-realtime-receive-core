import pytest

from realtime.receive.core import baseline_utils
from realtime.receive.core.baselines import Baselines


@pytest.mark.parametrize(
    "num_stations,lower_triangular,autocorr,expected_indices",
    (
        (1, True, False, ()),
        (1, True, True, ((0, 0),)),
        (1, False, False, ()),
        (1, False, True, ((0, 0),)),
        (
            4,
            True,
            False,
            ((1, 0), (2, 0), (2, 1), (3, 0), (3, 1), (3, 2)),
        ),
        (
            4,
            True,
            True,
            ((0, 0), (1, 0), (1, 1), (2, 0), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2), (3, 3)),
        ),
        (
            4,
            False,
            False,
            ((0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)),
        ),
        (
            4,
            False,
            True,
            ((0, 0), (0, 1), (0, 2), (0, 3), (1, 1), (1, 2), (1, 3), (2, 2), (2, 3), (3, 3)),
        ),
    ),
)
def test_baseline_indices(num_stations, autocorr, lower_triangular, expected_indices):
    indices = baseline_utils.baseline_indices(num_stations, lower_triangular, autocorr)
    assert expected_indices == indices

    baselines = Baselines.generate(num_stations, autocorr, lower_triangular)
    assert baselines.antenna1 == [i[0] for i in expected_indices]
    assert baselines.antenna2 == [i[1] for i in expected_indices]


@pytest.mark.parametrize(
    "antennas, autocorr, lower_triangular, expected_baselines",
    (
        (
            ("a", "b", "c"),
            True,
            True,
            (("a", "a"), ("b", "a"), ("b", "b"), ("c", "a"), ("c", "b"), ("c", "c")),
        ),
        (
            ("a", "b", "c"),
            True,
            False,
            (("a", "a"), ("a", "b"), ("a", "c"), ("b", "b"), ("b", "c"), ("c", "c")),
        ),
        (("a", "b", "c"), False, True, (("b", "a"), ("c", "a"), ("c", "b"))),
        (("a", "b", "c"), False, False, (("a", "b"), ("a", "c"), ("b", "c"))),
    ),
)
def test_generate_baseliens(antennas, autocorr, lower_triangular, expected_baselines):
    baselines = baseline_utils.generate_baselines(
        antennas, lower_triangular=lower_triangular, autocorr=autocorr
    )
    assert expected_baselines == baselines


@pytest.mark.parametrize("autocorr", (True, False))
@pytest.mark.parametrize("lower_triangular", (True, False))
def test_invalid_baselines(lower_triangular, autocorr):
    num_antennas = 6
    baselines = baseline_utils.baseline_indices(
        num_antennas, lower_triangular=lower_triangular, autocorr=autocorr
    )
    antenna1 = [baseline[0] for baseline in baselines]
    antenna2 = [baseline[1] for baseline in baselines]

    # original values work, as usual
    assert Baselines(antenna1=antenna1, antenna2=antenna2) is not None
    assert Baselines(antenna1=antenna2, antenna2=antenna1) is not None

    # remove any baseline, should always complain
    for i in range(len(antenna1)):
        incomplete_antenna1 = antenna1[:i] + antenna1[i + 1 :]  # noqa: E203
        incomplete_antenna2 = antenna2[:i] + antenna2[i + 1 :]  # noqa: E203
        with pytest.raises(ValueError):
            Baselines(incomplete_antenna1, incomplete_antenna2)

    # add a baseline anywhere, should always complain
    for i in range(len(antenna1)):
        longer_antenna1 = antenna1[:]
        longer_antenna1.insert(i, longer_antenna1[-1])
        longer_antenna2 = antenna2[:]
        longer_antenna2.insert(i, longer_antenna2[-1])
        with pytest.raises(ValueError):
            Baselines(longer_antenna1, longer_antenna2)

    # modify any baseline with new antenna, it should complain
    antenna_that_doesnt_exist = num_antennas
    for i in range(len(antenna1)):
        invalid_antenna1 = antenna1[:]
        invalid_antenna1[i] = antenna_that_doesnt_exist
        with pytest.raises(ValueError):
            Baselines(invalid_antenna1, antenna2)
    for i in range(len(antenna2)):
        invalid_antenna2 = antenna2[:]
        invalid_antenna2[i] = antenna_that_doesnt_exist
        with pytest.raises(ValueError):
            Baselines(antenna1, invalid_antenna2)
