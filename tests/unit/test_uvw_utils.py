#!/usr/bin/env python3
import datetime
from math import acos

import numpy
import pytest
from astropy import units
from astropy.coordinates import Angle
from astropy.time import Time
from astropy.utils import iers
from casacore import tables
from casacore.measures import measures
from casacore.quanta import quantity

from realtime.receive.core.common import untar
from realtime.receive.core.uvw_engine import UVWEngine
from realtime.receive.core.uvw_utils import get_uvw, get_uvw_J2000

iers.conf.auto_download = False

# pylint: disable=missing-function-docstring


@pytest.fixture
def from_ms():
    ms = "tests/data/low_sim.ms"
    untar(ms)

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    pos_1 = ants.getcell("POSITION", 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol("DELAY_DIR", 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol("TIME_CENTROID", 0, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    # Lets get the Time
    time = Time(time_centroid / (3600.0 * 24), format="mjd")
    print(type(time))
    print(time)
    print(time.value)
    # lets get the MS from the measurement set
    uvw_from_ms = main.getcol("UVW", 0, 1, 1)

    return [pos_0, pos_1, time, -1.0 * uvw_from_ms, target_ra, target_dec]


@pytest.fixture
def from_askap_ms():
    ms = "tests/data/1934_SB4094_b0_t0_ch0.ms"
    untar(ms)

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    pos_1 = ants.getcell("POSITION", 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol("DELAY_DIR", 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol("TIME", 313, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    print(target_ra.hms, target_dec.dms)

    # Lets get the Time
    print(target_ra)
    time = Time(time_centroid / (3600.0 * 24), format="mjd")
    print(type(time))
    print(time)
    print(time.value)

    # lets get the MS from the measurement set - this one has autos
    uvw_from_ms = main.getcol("UVW", 313, 1, 1)

    return [pos_0, pos_1, time, uvw_from_ms, target_ra, target_dec]


def test_non_precessed_uvw_against_ms_length_from_2000(from_ms):
    # lets get the HA and Declination
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]
    time = from_ms[2]
    uvw_from_ms = from_ms[3]
    target_ra = from_ms[4]
    target_dec = from_ms[5]

    uvw = get_uvw(pos_0, pos_1, time, target_ra, target_dec, swap_baselines=False)

    # lets get the differences

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])
    test_2 = numpy.array([uvw[0], uvw[1], uvw[2]])

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)
    diff = abs(len_1 - len_2)
    print("Baselines differ in length by: ", diff, " m ")
    assert diff < 1.0e-3


def test_nonprecessed_uvw_from_ms_angle_from_2000(from_ms):
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]
    time = from_ms[2]
    uvw_from_ms = from_ms[3]
    target_ra = from_ms[4]
    target_dec = from_ms[5]

    # lets get the HA and Declination

    uvw = get_uvw(pos_0, pos_1, time, target_ra, target_dec, swap_baselines=False)

    # lets get the differences

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])
    test_2 = numpy.array([uvw[0], uvw[1], uvw[2]])

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)

    unit_1 = test_1 / len_1
    unit_2 = test_2 / len_2

    cos_separation = unit_1.dot(unit_2)

    angle_sep = acos(cos_separation)
    print("Angular separation of baselines: ", angle_sep)
    assert angle_sep < 0.00029


def test_uvw_generation_using_casacore(from_askap_ms):
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time_val = from_askap_ms[2]
    uvw_from_ms = from_askap_ms[3]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    dm = measures()

    refant = dm.position(
        "itrf",
        quantity(pos_1[0], "m"),
        quantity(pos_1[1], "m"),
        quantity(pos_1[2], "m"),
    )

    dm.do_frame(refant)

    ra_measure = quantity(target_ra.to_string(unit=units.rad, decimal=True) + " rad")
    dec_measure = quantity(target_dec.to_string(unit=units.rad, decimal=True) + " rad")
    source = dm.direction("j2000", ra_measure, dec_measure)
    dm.do_frame(source)  # where are we looking
    time_val.format = "isot"
    print(time_val.value)
    time_datetime = datetime.datetime.fromisoformat(time_val.value[0])
    epoch_val = quantity(time_datetime.isoformat())

    epoch_measure = dm.epoch("ut1", epoch_val)

    dm.do_frame(epoch_measure)  # what time is it
    print(dm.get_value(epoch_measure))

    azel = dm.measure(source, "azel")

    print(dm.get_value(azel))

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])

    test_2 = get_uvw_J2000(
        pos_0,
        pos_1,
        pos_1,
        time_val,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="WGS84",
    )

    print(test_1)
    print(test_2)

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)

    unit_1 = test_1 / len_1
    unit_2 = test_2 / len_2

    cos_separation = unit_1.dot(unit_2)

    angle_sep = acos(cos_separation)
    print("Angular separation of baselines: ", angle_sep)
    assert angle_sep < 0.00029

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)
    diff = abs(len_1 - len_2)
    print("Baselines differ in length by: ", diff, " m ")
    assert diff < 1.0e-3


def do_compare(test_1, test_2):
    print(test_1)
    print(test_2)

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)

    unit_1 = test_1 / len_1
    unit_2 = test_2 / len_2

    cos_separation = unit_1.dot(unit_2)

    angle_sep = acos(cos_separation)
    print("Angular separation of baselines: ", angle_sep)
    assert angle_sep < 0.00029, "Angular separation greater than 1 minute of arc"

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)
    diff = abs(len_1 - len_2)
    print("Baselines differ in length by: ", diff, " m ")
    stretch = (len_1 / len_2) - 1.0
    print("Stretch: ", stretch)

    assert diff < 1.0e-3


def test_antenna_based_precessed_uvw_calculation(from_askap_ms):
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time = from_askap_ms[2]
    uvw_from_ms = from_askap_ms[3]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    uvw_1 = UVWEngine.get_antenna_uvw(
        pos_0,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="WGS84",
    )
    uvw_2 = UVWEngine.get_antenna_uvw(
        pos_1,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="WGS84",
    )

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])
    test_2 = uvw_1 - uvw_2

    do_compare(test_1, test_2)


def test_antenna_based_non_precessed_uvw_calculation(from_ms):
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]
    time = from_ms[2]
    uvw_from_ms = from_ms[3]
    target_ra = from_ms[4]
    target_dec = from_ms[5]

    uvw_1 = UVWEngine.get_antenna_uvw(
        pos_0,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="itrf",
    )
    uvw_2 = UVWEngine.get_antenna_uvw(
        pos_1,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="itrf",
    )

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])
    test_2 = uvw_1 - uvw_2

    do_compare(test_1, test_2)


def test_should_fail_compare_precessed_and_non_precessed_measures(
    from_askap_ms,
):
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time = from_askap_ms[2]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    uvw_1_precessed = UVWEngine.get_antenna_uvw(
        pos_0,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="itrf",
        epoch_frame="J2000",
    )
    uvw_2_precessed = UVWEngine.get_antenna_uvw(
        pos_1,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="itrf",
        epoch_frame="J2000",
    )

    uvw_1_ofdate = UVWEngine.get_antenna_uvw(
        pos_0,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="itrf",
        epoch_frame="OFDATE",
    )
    uvw_2_ofdate = UVWEngine.get_antenna_uvw(
        pos_1,
        time,
        target_ra,
        target_dec,
        swap_baselines=False,
        position_frame="itrf",
        epoch_frame="OFDATE",
    )

    test_1 = uvw_2_precessed - uvw_1_precessed
    test_2 = uvw_2_ofdate - uvw_1_ofdate

    with pytest.raises(AssertionError):
        do_compare(test_1, test_2)
