import errno
import logging
import random
import socket
from unittest.mock import Mock, patch

import pytest

from realtime.receive.core import socket_utils

logger = logging.getLogger(__name__)


def _mock_socket_class(max_rsize: int, max_wsize: int, as_linux: bool):
    class _MockSocket:
        def __init__(self, _af, kind):
            self._kind = kind
            self._max_rsize = max_rsize
            self._max_wsize = max_wsize
            self._as_linux = as_linux
            self._opts = {}

        def _validate_opt_value(self, value, max_valid_value):
            if value <= max_valid_value:
                return value
            if self._as_linux:
                return max_valid_value
            raise OSError(errno.ENOBUFS, "out of range")

        def setsockopt(self, level, opt, value):
            """mock setsockopt"""
            assert level == socket.SOL_SOCKET
            if opt == socket.SO_RCVBUF:
                value = self._validate_opt_value(value, self._max_rsize)
            elif opt == socket.SO_SNDBUF:
                value = self._validate_opt_value(value, self._max_wsize)
            if self._as_linux:
                value *= 2
            self._opts[opt] = value

        def getsockopt(self, level, opt):
            """mock getsockopt"""
            assert level == socket.SOL_SOCKET
            return self._opts[opt]

    return _MockSocket


@pytest.mark.parametrize("protocol", ("udp", "tcp"))
@pytest.mark.parametrize("as_linux", (True, False))
def test_max_socket_buffer_sizes_via_mock(protocol, as_linux):
    given_max_rsize = random.randint(1024, 1024 * 1024)
    given_max_wsize = random.randint(1024, 1024 * 1024)
    mock = _mock_socket_class(given_max_rsize, given_max_wsize, as_linux)
    platform = "linux" if as_linux else "not_linux"

    for force_socket_method in (True, False):
        # still check if it actually exists, otherwise we're not testing what
        # we think we are
        open_ = Mock(side_effect=FileNotFoundError()) if force_socket_method else open
        with (
            patch("socket.socket", new=mock),
            patch("sys.platform", new=platform),
            patch("builtins.open", open_),
        ):
            max_r_bufsize = socket_utils.max_socket_read_buffer_size(protocol)
            max_w_bufsize = socket_utils.max_socket_write_buffer_size(protocol)
            if force_socket_method:
                assert max_r_bufsize == given_max_rsize
                assert max_w_bufsize == given_max_wsize
            else:
                assert isinstance(max_r_bufsize, int)
                assert isinstance(max_w_bufsize, int)


@pytest.mark.parametrize("protocol", ("udp", "tcp"))
def test_max_socket_buffer_sizes_directly(protocol):
    assert socket_utils.max_socket_read_buffer_size(protocol) > 0
    assert socket_utils.max_socket_write_buffer_size(protocol) > 0
