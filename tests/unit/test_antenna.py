from typing import Tuple

import pytest
from astropy import units
from astropy.coordinates import Angle
from astropy.time import Time
from astropy.utils import iers
from casacore import tables
from numpy import absolute, array

from realtime.receive.core import Antenna, antenna_utils
from realtime.receive.core.common import untar

iers.conf.auto_download = False

# pylint: disable=missing-function-docstring


@pytest.fixture
def antenna() -> Tuple[Antenna]:
    all_antennas = antenna_utils.load_antennas("tests/data/test_antenna.json")
    return tuple(all_antennas)


@pytest.fixture
def from_ms():
    ms = "tests/data/low_sim.ms"
    untar(ms)

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    pos_1 = ants.getcell("POSITION", 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol("DELAY_DIR", 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol("TIME_CENTROID", 0, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    # Lets get the Time
    time = Time(time_centroid / (3600.0 * 24), format="mjd")

    # lets get the MS from the measurement set
    uvw_from_ms = main.getcol("UVW", 0, 1, 1)

    return [pos_0, pos_1, time, uvw_from_ms, target_ra, target_dec]


@pytest.fixture
def from_askap_ms():
    ms = "tests/data/1934_SB4094_b0_t0_ch0.ms"
    untar(ms)

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    pos_1 = ants.getcell("POSITION", 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol("DELAY_DIR", 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol("TIME", 313, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    # Lets get the Time

    time = Time(time_centroid / (3600.0 * 24), format="mjd")

    # lets get the MS from the measurement set - this one has autos
    uvw_from_ms = main.getcol("UVW", 313, 1, 1)

    return [pos_0, pos_1, time, uvw_from_ms, target_ra, target_dec]


def test_antenna(from_ms):
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = [
        {
            "interface": "https://schema.skao.int/ska-telmodel-layout-receptor-fixed-delay/0.0",
            "fixed_delay_id": "FIX_H",
            "polarisation": 0,
            "units": "m",
            "delay": 0.0,
        },
        {
            "interface": "https://schema.skao.int/ska-telmodel-layout-receptor-fixed-delay/0.0",
            "fixed_delay_id": "FIX_V",
            "polarisation": 0,
            "units": "m",
            "delay": 0.0,
        },
    ]
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    KPantenna_0 = antenna_0.as_KatPointAntenna()
    KPantenna_1 = antenna_1.as_KatPointAntenna(ref_location=KPantenna_0.location)

    assert (KPantenna_1.position_enu[0] - 47.30819061005508) < 1e-8


@pytest.mark.parametrize("antennas", ["antenna"])
def test_loaded_antenna(antennas, request):
    my_antennas = request.getfixturevalue(antennas)

    my_antenna = my_antennas[0]
    assert my_antenna.x == -2565044.433249
    assert my_antenna.y == 5085763.694774
    assert my_antenna.z == -2861062.439952
    assert my_antenna.lat == -26.824722084
    assert my_antenna.lon == 116.76444824
    assert my_antenna.h == 365.0
    assert my_antenna.label == "FAKE-TEST"
    assert my_antenna.name == "FAKE-TEST"
    assert my_antenna.fixed_delay_h == 100.0
    assert my_antenna.fixed_delay_v == 100.0
    assert my_antenna.niao == 99.0
    if antennas == "antenna":
        assert my_antenna.id == 56

    RefAntenna = my_antennas[1].as_KatPointAntenna()

    KPantenna_nodelay = my_antennas[0].as_KatPointAntenna()
    KPantenna_delay = my_antennas[0].as_KatPointAntenna(ref_location=RefAntenna.location)

    diff = array([0, 0, 0])

    diff[0] = (KPantenna_delay.location.x - KPantenna_nodelay.location.x).value
    diff[1] = (KPantenna_delay.location.y - KPantenna_nodelay.location.y).value
    diff[2] = (KPantenna_delay.location.z - KPantenna_nodelay.location.z).value

    assert all(absolute(diff) < 1e-18)

    assert KPantenna_delay.delay_model["POS_E"] == 133.73568790107748
    assert KPantenna_delay.delay_model["POS_N"] == 55.8367838302129
    assert KPantenna_delay.delay_model["POS_U"] == -1.0016463885330502

    assert KPantenna_delay.delay_model["FIX_H"] == 100.0
    assert KPantenna_delay.delay_model["FIX_V"] == 100.0
    assert KPantenna_delay.delay_model["NIAO"] == 99.0


def test_fail_load():
    with pytest.raises(ValueError):
        antenna_utils.load_antennas("tests/data/test_antenna_old.json")
