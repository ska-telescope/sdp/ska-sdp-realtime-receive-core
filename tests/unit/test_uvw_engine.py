from math import acos

import numpy
import pytest
from astropy import units
from astropy.coordinates import Angle
from astropy.time import Time, TimeDelta
from astropy.utils import iers
from casacore import tables

from realtime.receive.core import Antenna
from realtime.receive.core.common import untar
from realtime.receive.core.erfa_uvw_engine import ErfaUVWEngine
from realtime.receive.core.katpoint_uvw_engine import KatpointUVWEngine
from realtime.receive.core.scan import PhaseDirection
from realtime.receive.core.uvw_engine import UVWEngine
from realtime.receive.core.uvw_utils import get_uvw, get_uvw_J2000

iers.conf.auto_download = False

# pylint: disable=missing-function-docstring


@pytest.fixture
def from_ms():
    ms = "tests/data/low_sim.ms"
    untar(ms)

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    pos_1 = ants.getcell("POSITION", 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol("DELAY_DIR", 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol("TIME_CENTROID", 0, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    # Lets get the Time
    time = Time(time_centroid / (3600.0 * 24), format="mjd")
    print(type(time))
    print(time)
    print(time.value)
    # lets get the MS from the measurement set
    uvw_from_ms = -1.0 * (main.getcol("UVW", 0, 1, 1))
    # this simulation seems to have an inverted baseline. Lets fix that
    # Steve Ord.

    return [pos_0, pos_1, time, uvw_from_ms, target_ra, target_dec]


@pytest.fixture
def from_askap_ms():
    ms = "tests/data/1934_SB4094_b0_t0_ch0.ms"
    untar(ms)

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell("POSITION", 0)
    pos_1 = ants.getcell("POSITION", 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol("DELAY_DIR", 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol("TIME", 313, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    print(target_ra.hms, target_dec.dms)

    # Lets get the Time
    print(target_ra)
    time = Time(time_centroid / (3600.0 * 24), format="mjd")
    print(type(time))
    print(time)
    print(time.value)

    # lets get the MS from the measurement set - this one has autos
    uvw_from_ms = main.getcol("UVW", 313, 1, 1)

    return [pos_0, pos_1, time, uvw_from_ms, target_ra, target_dec]


def do_compare(test_1, test_2, tolerance_mm=1.0, tolerance_arcsec=40.0):
    print(test_1)
    print(test_2)

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)

    unit_1 = test_1 / len_1
    unit_2 = test_2 / len_2

    cos_separation = unit_1.dot(unit_2)

    angle_sep = acos(cos_separation)
    print("Angular separation of baselines: ", angle_sep)
    print("tolerance_radian: ", tolerance_arcsec * 4.8e-6)

    len_1 = numpy.linalg.norm(test_1)
    len_2 = numpy.linalg.norm(test_2)
    diff = abs(len_1 - len_2)
    print("Baselines differ in length by: ", diff, " m ")
    print("tolerance_m: ", tolerance_mm / 1000.0)
    stretch = (len_1 / len_2) - 1.0
    print("Stretch: ", stretch)

    assert diff < tolerance_mm / 1000.0
    assert angle_sep < tolerance_arcsec * 4.8e-6


def test_uvw_machine(from_askap_ms):
    """
    This test compares calcuated visibilites in the J2000 frame.
    THis requires all the machinery to work. In that the baselines need to
    be calculated for the 2000 epoch even though the date of the observation is not
    2000.

    So the look direction is precessed and the UVW are rotated.
    """
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time = from_askap_ms[2]
    uvw_from_ms = from_askap_ms[3]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = {}
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antennas = list((antenna_0, antenna_1))

    phase_direction = PhaseDirection(ra=target_ra, dec=target_dec, reference_time="2000.0")
    engine = UVWEngine(antennas=antennas)

    assert engine.num_stations() == 2
    assert engine.num_baselines() == 3

    uvw_from_utils = get_uvw_J2000(
        xyposA=pos_0,
        xyposB=pos_1,
        refpos=pos_0,
        epoch=time,
        ra=target_ra,
        dec=target_dec,
        swap_baselines=False,
    )

    uvw_from_engine = engine.get_uvw(
        time, phase_direction, swap_baselines=False, direction_frame="itrf"
    )

    assert uvw_from_engine.shape == (3, 3)

    # lets get the differences

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])

    test_2 = numpy.array([uvw_from_utils[0], uvw_from_utils[1], uvw_from_utils[2]])

    test_3 = numpy.array([uvw_from_engine[1][0], uvw_from_engine[1][1], uvw_from_engine[1][2]])

    do_compare(test_1, test_3)
    do_compare(test_2, test_3)


def test_uvw_machine_cache(from_ms):
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]
    time = from_ms[2]
    target_ra = from_ms[4]
    target_dec = from_ms[5]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = {}
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antennas = list((antenna_0, antenna_1))

    phase_direction = PhaseDirection(ra=target_ra, dec=target_dec, reference_time="2000.0")

    engine = UVWEngine(antennas=antennas)

    # lets give the time a kick

    time_delta = TimeDelta(100.0, format="sec")

    time2 = time + time_delta

    # get the UVW from the new time
    uvw_from_utils = get_uvw(pos_0, pos_1, time2, target_ra, target_dec, swap_baselines=False)

    # update the time in the cache - this should create a new
    # entry but keep the old one

    uvw_from_engine = engine.get_uvw(time=time2, phase_direction=phase_direction)

    assert uvw_from_engine is not None
    assert engine.num_entries() == 1

    # lets get the differences

    test_1 = numpy.array([uvw_from_utils[0][0], uvw_from_utils[1][0], uvw_from_utils[2][0]])

    test_2 = numpy.array([uvw_from_engine[1][0], uvw_from_engine[1][1], uvw_from_engine[1][2]])

    do_compare(test_1, test_2)

    # Now lets see if the old ones are still in the cache

    uvw_from_engine = engine.get_uvw(time, phase_direction)

    assert engine.num_entries() == 2
    # get the UVW from the new time
    uvw_from_utils = get_uvw(pos_0, pos_1, time, target_ra, target_dec, swap_baselines=False)

    # lets get the differences

    test_1 = numpy.array([uvw_from_utils[0][0], uvw_from_utils[1][0], uvw_from_utils[2][0]])

    test_2 = numpy.array([uvw_from_engine[1][0], uvw_from_engine[1][1], uvw_from_engine[1][2]])

    do_compare(test_1, test_2)


def test_uvw_machine_update_time(from_ms):
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]
    time = from_ms[2]
    target_ra = from_ms[4]
    target_dec = from_ms[5]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = {}
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antennas = list((antenna_0, antenna_1))

    phase_direction = PhaseDirection(ra=target_ra, dec=target_dec, reference_time="2000.0")
    engine = UVWEngine(antennas=antennas)

    assert engine.num_stations() == 2
    assert engine.num_baselines() == 3

    # lets give the time a kick

    time_delta = TimeDelta(100.0, format="sec")

    time = time + time_delta

    uvw_from_utils = get_uvw(pos_0, pos_1, time, target_ra, target_dec, swap_baselines=False)

    uvw_from_engine = engine.get_uvw(time, phase_direction=phase_direction)

    # lets get the differences

    test_1 = numpy.array([uvw_from_utils[0][0], uvw_from_utils[1][0], uvw_from_utils[2][0]])

    test_2 = numpy.array([uvw_from_engine[1][0], uvw_from_engine[1][1], uvw_from_engine[1][2]])

    do_compare(test_1, test_2)


@pytest.mark.parametrize(
    "engine,tolerance_mm,tolerance_arcsec",
    (
        (UVWEngine, 1.0, 40.0),
        (KatpointUVWEngine, 1.0, 40.0),
        (ErfaUVWEngine, 1.0, 50.0),
    ),
)
def test_compare_machines_w_askap(from_askap_ms, engine, tolerance_mm, tolerance_arcsec):
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time = from_askap_ms[2]
    uvw_from_ms = from_askap_ms[3]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = {}
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antennas = list((antenna_0, antenna_1))

    phase_direction = PhaseDirection(
        ra=target_ra, dec=target_dec, reference_time="", reference_frame="icrs"
    )

    engine_ut = engine(antennas=antennas)

    uvw_from_eut = engine_ut.get_uvw(time, phase_direction=phase_direction)

    # lets get the differences

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])
    test_2 = numpy.array([uvw_from_eut[1][0], uvw_from_eut[1][1], uvw_from_eut[1][2]])
    do_compare(test_1, test_2, tolerance_mm=tolerance_mm, tolerance_arcsec=tolerance_arcsec)


@pytest.mark.parametrize(
    "engine,tolerance_mm,tolerance_arcsec",
    (
        (UVWEngine, 1.0, 1.0),
        (ErfaUVWEngine, 1.0, 30.0),
    ),
)
def test_compare_machines_w_katpoint(from_askap_ms, engine, tolerance_mm, tolerance_arcsec):
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time = from_askap_ms[2]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = {}
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antennas = list((antenna_0, antenna_1))

    phase_direction = PhaseDirection(
        ra=target_ra, dec=target_dec, reference_time="", reference_frame="icrs"
    )

    engine_ut = engine(antennas=antennas)
    katpoint_engine = KatpointUVWEngine(antennas=antennas)
    uvw_from_katpoint = katpoint_engine.get_uvw(time, phase_direction=phase_direction)
    uvw_from_eut = engine_ut.get_uvw(time, phase_direction=phase_direction)

    # lets get the differences

    test_1 = numpy.array(
        [uvw_from_katpoint[1][0], uvw_from_katpoint[1][1], uvw_from_katpoint[1][2]]
    )
    test_2 = numpy.array([uvw_from_eut[1][0], uvw_from_eut[1][1], uvw_from_eut[1][2]])

    do_compare(test_1, test_2, tolerance_mm=tolerance_mm, tolerance_arcsec=tolerance_arcsec)


# In the lower triangle case, the first non-auto baseline is 1,0
# And in the standard case it is 0,1 - testing this using the
# swap_baselines flag
@pytest.mark.parametrize(
    "lower_tri_baselines,swap_baselines,baselines",
    (
        (True, True, None),
        (False, False, None),
        (
            False,
            True,
            ((0, 0), (1, 0), (1, 1)),
        ),
    ),
)
def test_compare_machines(from_ms, lower_tri_baselines, swap_baselines, baselines):
    pos_0 = from_ms[0]
    pos_1 = from_ms[1]
    time = from_ms[2]
    uvw_from_ms = from_ms[3]
    target_ra = from_ms[4]
    target_dec = from_ms[5]

    location_0 = {"geocentric": {"x": pos_0[0], "y": pos_0[1], "z": pos_0[2]}}

    location_1 = {"geocentric": {"x": pos_1[0], "y": pos_1[1], "z": pos_1[2]}}
    fixed_delays = {}
    niao = 0.0

    antenna_0 = Antenna(
        interface="test",
        station_label="001",
        station_id=1,
        diameter=35.0,
        location=location_0,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antenna_1 = Antenna(
        interface="test",
        station_label="002",
        station_id=2,
        diameter=35.0,
        location=location_1,
        fixed_delays=fixed_delays,
        niao=niao,
    )

    antennas = list((antenna_0, antenna_1))

    phase_direction = PhaseDirection(ra=target_ra, dec=target_dec, reference_time="2000.0")

    uvw_engine = UVWEngine(
        antennas=antennas,
        baselines=baselines,
        lower_triangular=lower_tri_baselines,
        swap_baselines=swap_baselines,
    )
    katpoint_engine = KatpointUVWEngine(
        antennas=antennas,
        baselines=baselines,
        lower_triangular=lower_tri_baselines,
        swap_baselines=swap_baselines,
    )

    uvw_from_engine = uvw_engine.get_uvw(time, phase_direction=phase_direction)
    uvw_from_katpoint = katpoint_engine.get_uvw(time, phase_direction=phase_direction)

    # lets get the differences

    test_1 = numpy.array([uvw_from_ms[0][0], uvw_from_ms[0][1], uvw_from_ms[0][2]])
    test_2 = numpy.array([uvw_from_engine[1][0], uvw_from_engine[1][1], uvw_from_engine[1][2]])

    test_3 = numpy.array(
        [
            uvw_from_katpoint[1][0],
            uvw_from_katpoint[1][1],
            uvw_from_katpoint[1][2],
        ]
    )

    do_compare(test_1, test_2)
    do_compare(test_1, test_3)
    do_compare(test_2, test_3)
