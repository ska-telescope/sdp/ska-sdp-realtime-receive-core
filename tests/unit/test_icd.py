import datetime
import time

import numpy as np
import pytest

from realtime.receive.core import time_utils
from realtime.receive.core.baseline_utils import baselines
from realtime.receive.core.icd import ItemDescription, ItemSendingContext, LowICD, MidICD, Payload


@pytest.mark.parametrize(
    "dtype, value, expected",
    [
        ("i8", 1.24, 1),
        ("u4", 1.24, np.array(1, dtype="u4")),
        ("u8", np.array([1.5, 2.5]), np.array([1, 2], dtype="u8")),
        ("i8", "123", 123),
        ("u4", "abc", ValueError()),
        ("f8", 1, 1.0),
        ("f4", 1, np.array(1.0, dtype="f4")),
        ("f8", np.array([1, 2]), np.array([1.0, 2.0], dtype="f8")),
        ("f8", "123", 123.0),
        ("f4", "abc", ValueError()),
    ],
)
def test_item_description_dtype_casting(dtype: str, value, expected):
    desc = ItemDescription(0x01, "test", dtype, ItemSendingContext.SOS_HEAP)
    if isinstance(expected, ValueError):
        with pytest.raises(ValueError):
            desc.cast_value(value)
    else:
        np.testing.assert_array_equal(expected, desc.cast_value(value), strict=True)


def test_mid_icd_time_to_mjd():
    unix_time = MidICD.icd_to_unix(640410866, 0)
    mjd = time_utils.unix_to_mjd(unix_time)
    assert mjd == 5093812434.0, "MJD sec should be 5093812434.0 for 2020-04-17T03:53:54+00:00"


def test_mid_icd_time_to_unix():
    # 1.5 secs after UNIX epoch
    assert pytest.approx(1.5, abs=1e-6) == MidICD.icd_to_unix(-946684767, 2**31)


def test_low_icd_time_to_unix():
    LEAP_SECONDS_AT_START_OF_2000 = 32
    unix_time = LowICD.icd_to_unix(1, 5e8)  # 1.5 secs after TAI's start of 2000
    assert (
        unix_time
        - datetime.datetime(
            2000,
            1,
            1,
            second=1,
            microsecond=500000,
            tzinfo=datetime.timezone.utc,
        ).timestamp()
        - LEAP_SECONDS_AT_START_OF_2000
    )


def test_mid_icd_time_roundtrips():
    """
    Checks that MidICD has self-consistent unix <--> ICD time conversion
    routines
    """
    now = time.time()
    assert pytest.approx(now, 1e-6) == MidICD.icd_to_unix(*MidICD.unix_to_icd(now))


def test_low_icd_time_roundtrips_without_significant_precision_loss():
    # Arbitrary point, but we choose 60s ago so that the offset calculated
    # below will have a non-trivial value
    now = time.time()
    icd_epoch = LowICD.sps_to_icd_epoch(LowICD.unix_to_sps(now - 60))
    icd_offset = LowICD.calc_icd_offset(LowICD.unix_to_sps(now), icd_epoch)
    assert LowICD.icd_to_unix(icd_epoch, icd_offset) - now < 1e-6


def test_icd_epoch_correctly_truncates_to_the_second():
    now = time.time()
    sps_now = LowICD.unix_to_sps(now)

    icd_epoch = LowICD.sps_to_icd_epoch(sps_now)
    assert sps_now - icd_epoch < 1

    icd_offset = LowICD.calc_icd_offset(sps_now, icd_epoch)
    assert icd_offset < 1e9


def test_cci():
    payload = Payload()
    random_cci = np.random.rand(10)
    payload.cci = random_cci
    assert np.array_equal(random_cci, payload.cci)


def test_size_utility_methods():
    # Mid sends CCIs, which are 1 byte long
    assert MidICD.corr_out_data_row_size() - LowICD.corr_out_data_row_size() == 1

    # Data heaps are comprised of more than just the correlator output data item
    test_cases = tuple((LowICD, num_stations, 1) for num_stations in (6, 16, 128, 512)) + tuple(
        (MidICD, num_stations, channels_per_stream)
        for num_stations in (1, 4, 8, 64, 192)
        for channels_per_stream in (1, 20)
    )
    for ICD, num_stations, channels_per_stream in test_cases:
        num_baselines = baselines(num_stations, autocorr=True)
        corr_out_data_size = ICD.corr_out_data_size(num_baselines, channels_per_stream)
        data_heap_size = ICD.data_heap_size(num_baselines, channels_per_stream)
        assert data_heap_size > corr_out_data_size

    # Assorted examples from real life
    real_size_examples = (
        (MidICD, 1, 20, 700),
        (MidICD, 4, 20, 7000),
        (LowICD, 6, 1, 714),
    )
    for ICD, num_stations, channels_per_stream, expected_corr_out_data_size in real_size_examples:
        num_baselines = baselines(num_stations, autocorr=True)
        corr_out_data_size = ICD.corr_out_data_size(num_baselines, channels_per_stream)
        assert corr_out_data_size == expected_corr_out_data_size
