import numpy as np
import pytest
from numpy.testing import assert_almost_equal, assert_array_almost_equal, assert_array_equal

from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.scan import SpectralWindow

# [105, 115, 125, ...], channel_width = 10, channel_bandwidth = 10
SINGLE_STRIDE_SW = SpectralWindow(
    "0",
    count=10,
    start=0,
    freq_min=100.0,
    freq_max=200.0,
    stride=1,
)
# [105, 115, 125, ...], channel_width = 10, channel_bandwidth = 5
DOUBLE_STRIDE_SW = SpectralWindow(
    "0",
    count=10,
    start=0,
    freq_min=102.5,
    freq_max=197.5,
    stride=2,
)
# [105, 115, 125, ...], channel_width = 10, channel_bandwidth=3.33
TRIPLE_STRIDE_SW = SpectralWindow(
    "0",
    count=10,
    start=0,
    freq_min=103 + 1 / 3,
    freq_max=196 + 2 / 3,
    stride=3,
)


@pytest.mark.parametrize("sw", [SINGLE_STRIDE_SW, DOUBLE_STRIDE_SW, TRIPLE_STRIDE_SW])
@pytest.mark.parametrize("expected_width", [10])
def test_channel_width(sw: SpectralWindow, expected_width: float):
    assert_almost_equal(sw.channel_width, expected_width)


@pytest.mark.parametrize("sw", [SINGLE_STRIDE_SW, DOUBLE_STRIDE_SW, TRIPLE_STRIDE_SW])
@pytest.mark.parametrize("expected_freq", [np.linspace(105, 195, 10)])
def test_frequencies(sw: SpectralWindow, expected_freq: list[float]):
    frequencies = sw.frequencies
    assert len(frequencies) == sw.count
    assert_array_almost_equal(frequencies, expected_freq)


@pytest.mark.parametrize(
    "sw, expected_freq",
    [
        (SINGLE_STRIDE_SW, np.linspace(105, 195, 10)),
        (DOUBLE_STRIDE_SW, np.linspace(105, 195, 19)),
        (TRIPLE_STRIDE_SW, np.linspace(105, 195, 28)),
    ],
)
def test_unstrided_channels(sw: SpectralWindow, expected_freq: list[float]):
    # pylint: disable-next=protected-access
    assert_array_almost_equal(sw._unstrided_channels, expected_freq)


@pytest.mark.parametrize(
    "sw, expected_count",
    [
        (SINGLE_STRIDE_SW, 10),
        (DOUBLE_STRIDE_SW, 19),
        (TRIPLE_STRIDE_SW, 28),
    ],
)
def test_sw_unstrided_channel_count(sw: SpectralWindow, expected_count: int):
    # pylint: disable-next=protected-access
    assert expected_count == sw._unstrided_channel_count


@pytest.mark.parametrize(
    "sw, expected_bw",
    [
        (SINGLE_STRIDE_SW, 10),
        (DOUBLE_STRIDE_SW, 5),
        (TRIPLE_STRIDE_SW, 3 + 1 / 3),
    ],
)
def test_sw_channel_bandwidth(sw: SpectralWindow, expected_bw: float):
    assert_almost_equal(sw.channel_bandwidth, expected_bw)


@pytest.mark.parametrize(
    "sw, channel_range, expected_frequencies",
    [
        (SINGLE_STRIDE_SW, ChannelRange(1, 4, 2), [115, 135, 155, 175]),
        (DOUBLE_STRIDE_SW, ChannelRange(4, 3, 2), [125, 135, 145]),
        (TRIPLE_STRIDE_SW, ChannelRange(9, 2, 6), [135, 155]),
        # Smoke test for !contains_range()
        (DOUBLE_STRIDE_SW, ChannelRange(1, 2), AssertionError),
        (TRIPLE_STRIDE_SW, ChannelRange(0, 2, 2), AssertionError),
    ],
)
def test_frequencies_for_range(
    sw: SpectralWindow,
    channel_range: ChannelRange,
    expected_frequencies: list[float] | AssertionError,
):
    if isinstance(expected_frequencies, list):
        assert_array_equal(sw.frequencies_for_range(channel_range), expected_frequencies)
    else:
        with pytest.raises(expected_frequencies):
            sw.frequencies_for_range(channel_range)


def _sw(start: int, count: int, stride=1):
    return SpectralWindow(
        spectral_window_id="0",
        count=count,
        start=start,
        freq_min=0.0,
        freq_max=10.0,
        stride=stride,
    )


@pytest.mark.parametrize(
    "sw, start_id, count, expected_channels",
    [
        (_sw(100, 10, 2), None, None, ChannelRange(100, 10, 2)),
        (_sw(100, 10, 2), 102, None, ChannelRange(102, 9, 2)),
        (_sw(100, 10, 2), None, 8, ChannelRange(100, 8, 2)),
        (_sw(100, 10, 2), 102, 8, ChannelRange(102, 8, 2)),
        (_sw(100, 10, 2), 108, 1, ChannelRange(108, 1, 2)),
        (_sw(100, 10, 3), 99, None, None),
        (_sw(100, 10, 3), 101, None, None),
        (_sw(100, 10, 3), 128, None, None),
        (_sw(100, 10, 3), 130, None, None),
        (_sw(100, 10, 3), None, 11, None),
        (_sw(100, 10, 3), 103, 10, None),
    ],
)
def test_try_as_channel_range(
    sw: SpectralWindow,
    start_id: int | None,
    count: int | None,
    expected_channels: ChannelRange | None,
):
    channels = sw.try_as_channel_range(start_id, count)
    assert expected_channels == channels
