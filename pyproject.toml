
[build-system]
build-backend = "poetry.core.masonry.api"
requires = ["poetry-core>=1.8.2,<2"]

[project]
name = "ska-sdp-realtime-receive-core"
version = "7.0.0"
description = "Core utilities for the CBF emulator and realtime receive modules"
authors = [
    { name = "Stephen Ord", email = "stephen.ord@csiro.au" },
    { name = "Rodrigo Tobar", email = "rtobar@icrar.org" },
    { name = "Callan Gray", email = "callan.gray@icrar.org" },
]

[tool.poetry]
name = "ska-sdp-realtime-receive-core"
version = "7.0.0"
description = "Core utilities for the CBF emulator and realtime receive modules"
authors = [
    "Stephen Ord <stephen.ord@csiro.au>",
    "Rodrigo Tobar <rtobar@icrar.org>",
    "Callan Gray <callan.gray@icrar.org>",
]
license = "BSD-3-Clause"
packages = [{ include = "realtime", from = "src" }]

[tool.poetry.scripts]
ms-asserter = "realtime.receive.core.ms_asserter:main"

[[tool.poetry.source]]
name = "PyPI"
priority = "primary"

[[tool.poetry.source]]
name = "skao"
url = "https://artefact.skao.int/repository/pypi-internal/simple"
priority = "supplemental"

[tool.poetry.dependencies] # until poetry adopts https://peps.python.org/pep-0631/
python = "^3.10"
dataclass-type-validator = "*"
katpoint = "== 1.0a2"
astropy = ">=4.3"
numpy = "^1.19"
overrides = "^7.0"
python-casacore = ">= 3.1.1"

[tool.poetry.group.lint.dependencies]
black = ">=22.10.0"
flake8 = ">=3.8.4"
isort = ">=5.6.4"
pylint = "^2.17.0"
pylint_junit = ">=0.3.2"
toml-sort = ">=0.20.1"

[tool.poetry.group.test.dependencies]
pytest = "^7.1.0"
pytest-bdd = "^6.1.0"
pytest-cov = "^4.0.0"
pytest-asyncio = "^0.21.1"
pytest-benchmark = "^4.0.0"

[tool.poetry.group.docs.dependencies]
markupsafe = "^2.1"
pygments = ">=2.13"
recommonmark = ">=0.7.1"
sphinx = "^7.2.6"
sphinx-autobuild = ">=2021.3.14"
sphinx_pyproject = ">=0.1.0"
sphinx_rtd_theme = ">=1.1.1"
sphinxcontrib-websupport = ">=1.2.4"
enum_tools = { version = "^0.11.0", extras = ["sphinx"] }

[tool.pylint.MASTER]
ignore = 'version.py'

[tool.pylint.BASIC]
# Don't require docs on private or test methods/classes
no-docstring-rgx = '^_|^test_|^Test'

[tool.pylint.SIMILARITIES]
ignore-imports = 'no'

[tool.pylint.TYPECHECK]
ignored-classes = [
    'optparse.Values',
    'thread._local',
    '_thread._local',
    'astropy.units',
]

[tool.pylint.MISCELLANEOUS]
notes = ['FIXME', 'XXX']

[tool.pylint.'MESSAGES CONTROL']
disable = [
    'too-few-public-methods',
    'too-many-arguments',
    'too-many-locals',
    'too-many-instance-attributes',
    'too-many-public-methods',
    'too-many-return-statements',
    'too-many-statements',
    'duplicate-code',
    'no-else-return',
    'no-else-continue',
    'redefined-outer-name',
    'invalid-name',
    'missing-module-docstring',
    # We handle this with isort
    'wrong-import-order',
    'ungrouped-imports',
    # Black sometimes allows lines that are longer (e.g. to avoid breaking string)
    'line-too-long',
]

[tool.isort]
profile = "black"
line_length = 99

[tool.black]
line-length = 99

[tool.pytest.ini_options]
testpaths = "tests"
junit_family = "xunit1"
addopts = [
    "--show-capture=all",
    "--log-level=INFO",
    "--cov=src",
    "--cov-append",
    "-vv",
    "--gherkin-terminal-reporter",
    "--cov-report",
    "term",
    "--cov-report",
    "html",
    "--cov-report",
    "xml",
    "--junitxml=./build/reports/unit-tests.xml",
    "--cucumber-json=./build/cucumber.json",
    "--benchmark-skip",
]
filterwarnings = [
	"ignore:.*XTP-.*",
]

[tool.coverage.run]
branch = true
