from typing import TypeVar

import numpy as np
from astropy.time import Time

T = TypeVar("T", float, np.floating, np.ndarray)

_SECS_PER_DAY = 86400.0
_UNIX_EPOCH_AS_MJD = 40587.0
_UNIX_EPOCH_AS_MJDS = _UNIX_EPOCH_AS_MJD * _SECS_PER_DAY


def mjd_to_icd(mjd: T) -> T:
    """MJD secs -> atomic seconds since ICD 2000 TAI epoch."""
    return mjd - 4453401568.0


def skao_to_mjd(mjd: T) -> T:
    """Atomic seconds since ICD 2000 TAI epoch. -> MJD secs."""
    return mjd + 4453401568.0


def unix_to_icd(unix: T) -> T:
    """Atomic seconds since UNIX epoch -> atomic seconds since ICD 2000 TAI epoch."""
    return unix - 946684768.0


def icd_to_unix(unix: T) -> T:
    """Atomic seconds since ICD 2000 TAI epoch -> atomic seconds since UNIX epoch."""
    return unix + 946684768.0


def mjd_to_unix(mjd: T) -> T:
    """MJD secs -> fractional secs since UNIX epoch."""
    return mjd - _UNIX_EPOCH_AS_MJDS


def unix_to_mjd(times: T) -> T:
    """Fractional secs since UNIX epoch -> MJD secs."""
    return times + _UNIX_EPOCH_AS_MJDS


def unix_as_astropy(time) -> Time:
    """UNIX timestamp -> astropy.time.Time"""
    return Time(time, format="unix")
