from typing import Dict, Optional, Sequence, Tuple, Union

from realtime.receive.core.antenna import Antenna
from realtime.receive.core.common import from_dict, load_json_resource


def _load_antennas_from_dicts(
    antennas_dicts: Sequence[Dict],
) -> Tuple[Antenna]:
    return tuple(from_dict(Antenna, data=data) for data in antennas_dicts)


def _load_antennas_from_file(antennas_file: str) -> Optional[Tuple[Antenna]]:
    """
    load the antennas from a json dictionary. It is possible
    that the interface may have changed.

    TODO:@steve-ord maybe add some schema validation


    """
    antennas_dict = load_json_resource(antennas_file)

    if "antennas" in antennas_dict:
        return _load_antennas_from_dicts(antennas_dict["antennas"])
    if "receptors" in antennas_dict:
        return _load_antennas_from_dicts(antennas_dict["receptors"])
    return None


def load_antennas(antennas: Union[str, Sequence[Dict]]) -> Tuple[Antenna]:
    """
    Loads antennas from a sequence of dictionaries, or a file containing them.

    :param antennas: A sequence of dictionaries, each capable of constructing
      an Antenna object. If a string is given instead, it is the route to a
      file with a JSON object whose top-level object has an "antennas" member
      with such a sequence of dictionaries.
    """
    if isinstance(antennas, str):
        return _load_antennas_from_file(antennas)
    return _load_antennas_from_dicts(antennas)
