import errno
import logging
import socket
import sys
from typing import Literal

Protocol = Literal["tcp", "udp"]
Mode = Literal["w", "r"]


logger = logging.getLogger(__name__)


def _max_socket_buffer_size_from_file(protocol: Protocol, mode: Mode) -> int:
    try:
        with open(f"/proc/sys/net/core/{mode}mem_max", "rb") as core_max_file:
            core_max = int(core_max_file.read().strip())
        proto_prefix = f"tcp_{mode}" if protocol == "tcp" else "udp_"
        with open(f"/proc/sys/net/ipv4/{proto_prefix}mem", "rb") as proto_limits_file:
            proto_max = int(proto_limits_file.read().strip().split()[2])
        return min(core_max, proto_max)
    except FileNotFoundError:
        # Others OSs don't have this exact same facility,
        # and even in Linux these values are not namespaced,
        # and not available for querying within a container
        return _max_socket_buffer_size_from_socket(protocol, mode)


def _get_sock_buffer_size(sock, sock_opt):
    result = sock.getsockopt(socket.SOL_SOCKET, sock_opt)
    # Linux doubles buffer size set via setsockopt
    if sys.platform == "linux":
        result //= 2
    return result


def _max_socket_buffer_size_from_socket(protocol: Protocol, mode: Mode) -> int:
    kind = socket.SOCK_STREAM if protocol == "tcp" else socket.SOCK_DGRAM
    sock_opt = socket.SO_SNDBUF if mode == "w" else socket.SO_RCVBUF
    sock = socket.socket(socket.AF_INET, kind)

    # SO_SNDBUF/SO_RCVBUF get an int value, so we bisect between 1 and 2^31-1
    # https://pubs.opengroup.org/onlinepubs/000095399/functions/setsockopt.html
    # search range is [low, high)
    high = 2**31
    low = 1
    # Start with a good guess, default max in Linux is 208kB
    guess = 208 * 1024
    result = None

    while True:
        try:
            sock.setsockopt(socket.SOL_SOCKET, sock_opt, guess)
        except OSError as e:
            if e.errno == errno.ENOBUFS:
                # closed down on the result
                if low + 1 == high:
                    break
                high = guess
                guess = low + (high - low) // 2
                continue
            raise
        result = _get_sock_buffer_size(sock, sock_opt)
        # Linux silently trims to maximum
        if result < guess:
            break
        # closed down on the result
        if guess + 1 == high:
            break
        low = guess + 1
        guess = low + (high - low) // 2

    return result


def max_socket_read_buffer_size(protocol: Protocol) -> int:
    """
    The maximum socket read buffer size allowed by the OS for the given
    protocol.
    """
    return _max_socket_buffer_size_from_file(protocol, "r")


def max_socket_write_buffer_size(protocol: Protocol) -> int:
    """
    The maximum socket read buffer size allowed by the OS for the given
    protocol.
    """
    return _max_socket_buffer_size_from_file(protocol, "w")
