import argparse
import unittest
from dataclasses import astuple, dataclass
from typing import List, Optional

import numpy as np

from realtime.receive.core.common import strtobool
from realtime.receive.core.msutils import MeasurementSet, Mode, _subtable


@dataclass
class AssertProps:
    """Table column properties to assert"""

    column_name: str  # table column name
    check_shape: bool  # set to true to assert column shape
    check_values: bool  # set to true to assert the column values


class MSAsserter(unittest.TestCase):
    """A class used to perform assertions on Measurement Sets"""

    def assert_ms_integrity(self, mspath: str):
        """
        Asserts the integrity of foreign key relations in ms tables
        according to MSv2 Schema.
        """
        ms = MeasurementSet.open(mspath, Mode.READONLY)

        ddtable = _subtable(ms.t, "DATA_DESCRIPTION", True)
        attable = _subtable(ms.t, "ANTENNA", True)
        swtable = _subtable(ms.t, "SPECTRAL_WINDOW", True)
        ptable = _subtable(ms.t, "POLARIZATION", True)
        ftable = _subtable(ms.t, "FIELD", True)
        otable = _subtable(ms.t, "OBSERVATION", True)
        stable = _subtable(ms.t, "STATE", True)
        prtable = _subtable(ms.t, "PROCESSOR", True)

        def _assert_bounds(start, end, value):
            assert start <= value < end

        # check foreign keys
        # maintable
        for rownr in range(ms.t.nrows()):
            _assert_bounds(0, attable.nrows(), ms.t.getcell("ANTENNA1", rownr))
            _assert_bounds(0, attable.nrows(), ms.t.getcell("ANTENNA2", rownr))
            _assert_bounds(0, ddtable.nrows(), ms.t.getcell("DATA_DESC_ID", rownr))
            _assert_bounds(0, ftable.nrows(), ms.t.getcell("FIELD_ID", rownr))
            _assert_bounds(0, otable.nrows(), ms.t.getcell("OBSERVATION_ID", rownr))
            _assert_bounds(0, stable.nrows(), ms.t.getcell("STATE_ID", rownr))
            _assert_bounds(0, prtable.nrows(), ms.t.getcell("PROCESSOR_ID", rownr))
            # ms.t.getcell("ARRAY_ID", rownr) array table is optional
        # data desc table
        for rownr in range(ddtable.nrows()):
            _assert_bounds(0, ptable.nrows(), ddtable.getcell("POLARIZATION_ID", rownr))
            _assert_bounds(
                0,
                swtable.nrows(),
                ddtable.getcell("SPECTRAL_WINDOW_ID", rownr),
            )

    def assert_ms_equal(
        self,
        first_ms: str,
        second_ms: str,
        columns: List[AssertProps],
        column_desc_keys: Optional[List[str]] = None,
        num_repeats: int = 1,
    ):
        """
        Asserts equality of two measurements sets on the specified
        columns and column desc keys.
        """

        in_ms = MeasurementSet.open(first_ms, Mode.READONLY)
        out_ms = MeasurementSet.open(second_ms, Mode.READONLY)
        self.assertEqual(in_ms.num_channels, out_ms.num_channels)  # pylint: disable=no-member
        self.assertEqual(in_ms.num_pols, out_ms.num_pols)  # pylint: disable=no-member
        self.assertEqual(
            in_ms.num_rows * num_repeats, out_ms.num_rows
        )  # pylint: disable=no-member
        self.assertEqual(in_ms.num_stations, out_ms.num_stations)  # pylint: disable=no-member

        for column in columns:
            column_name, check_shape, check_values = astuple(column)
            if column_desc_keys is None:
                # Assert all keys
                self.assertEqual(
                    in_ms.t.getcoldesc(column_name),
                    out_ms.t.getcoldesc(column_name),
                )
            else:
                for key in column_desc_keys:
                    # Assert specific keys only
                    if key in in_ms.t.getcoldesc(column_name) or key in out_ms.t.getcoldesc(
                        column_name
                    ):
                        self.assertTrue(
                            key in in_ms.t.getcoldesc(column_name),
                            f"first ms does not contain {column_name} coldesc {key}",
                        )
                        self.assertTrue(
                            key in out_ms.t.getcoldesc(column_name),
                            f"second ms does not contain {column_name} coldesc {key}",
                        )
                        msg = f"measurement sets table descriptions differ at {column_name} coldesc {key}"
                        if key == "shape":
                            np.testing.assert_array_equal(
                                in_ms.t.getcoldesc(column_name)[key],
                                out_ms.t.getcoldesc(column_name)[key],
                                msg,
                            )
                        else:
                            self.assertEqual(
                                in_ms.t.getcoldesc(column_name)[key],
                                out_ms.t.getcoldesc(column_name)[key],
                                msg,
                            )
            if check_shape:
                expected_shape = np.asarray(in_ms.read_column(column_name).shape)
                expected_shape[0] *= num_repeats
                np.testing.assert_array_equal(
                    expected_shape,
                    out_ms.read_column(column_name).shape,
                    f"column {column_name} shape does not match",
                )
            if check_values:
                for n in range(0, num_repeats):
                    np.testing.assert_array_equal(
                        in_ms.read_column(column_name),
                        out_ms.read_column(column_name, in_ms.num_rows * n, in_ms.num_rows),
                        f"column {column_name} does not match",
                    )

    def assert_ms_data_equal(self, first_ms: str, second_ms: str, num_repeats=1):
        """
        Asserts that first_ms and second_ms are roughly equal by comparing the
        number of channels, polarizations, rows and stations they store, and
        by fully comparing their DATA column
        """

        # This is a minimal set of ms metadata and schema to be written for post processing
        self.assert_ms_equal(
            first_ms,
            second_ms,
            num_repeats=num_repeats,
            columns=[
                AssertProps("DATA", True, True),
                AssertProps("FLAG", True, False),
                AssertProps("WEIGHT", True, False),
                AssertProps("SIGMA", True, False),
                AssertProps("UVW", True, False),
                AssertProps("TIME", True, False),
                AssertProps("ANTENNA1", True, False),
                AssertProps("ANTENNA2", True, False),
            ],
            column_desc_keys=[
                "ndim",
                # 'keywords', testdata doesn't have units
                "valueType",
                "_c_order",
                "maxlen",
                # 'shape',
                # 'option',
                # 'dataManagerGroup',
                # 'comment',
            ],
        )

    def assert_heaps_dropped(self, first_ms, second_ms):
        """
        Asserts that first_ms and second_ms are roughly equal by comparing the
        number of channels, polarizations, rows and stations they store, and
        but assert that some of the vis and flags are zero
        """

        in_ms = MeasurementSet.open(first_ms, Mode.READONLY)
        out_ms = MeasurementSet.open(second_ms, Mode.READONLY)
        self.assertEqual(in_ms.num_channels, out_ms.num_channels)  # pylint: disable=no-member
        self.assertEqual(in_ms.num_pols, out_ms.num_pols)  # pylint: disable=no-member
        self.assertEqual(in_ms.num_rows, out_ms.num_rows)  # pylint: disable=no-member
        self.assertEqual(in_ms.num_stations, out_ms.num_stations)  # pylint: disable=no-member

        # this just pulls out all the channels of the first visibility
        # I have forced all of the HEAPS for the first channels to be dropped
        # this checks if the any of visibilities do not match.
        # it also asserts that the first channel has zero weight - but the others do not
        first_vis_in, first_vis_out = (
            ms.read_vis(0, 0, ms.num_channels, 1) for ms in (in_ms, out_ms)
        )
        first_flags_in, first_flags_out = (
            ms.read_flags(0, 0, ms.num_channels, 1)  # pylint: disable=unused-variable
            for ms in (in_ms, out_ms)
        )

        # Verifies that the 1st channel has been completely dropped
        assert first_flags_out[:, 0].all(), first_flags_out

        # Dropped data should be reflected in the flag table and vis set to 0
        for channel in range(in_ms.num_channels):
            if not np.array_equal(first_vis_in[:, channel], first_vis_out[:, channel]):
                assert not first_flags_in[:, channel].all()
                assert first_flags_out[:, channel].all()
                assert not first_vis_out[:, channel].all()


def main():
    """CLI entrypoint for asserting two measurement sets are the same"""

    parser = argparse.ArgumentParser(description="Runs assertions for measurement sets")
    parser.add_argument(
        "filename1",
        help="Absolute or relative path of the correct measurement set",
    )
    parser.add_argument(
        "filename2",
        help="Absolute or relative path of the asserted measurement set",
    )
    parser.add_argument(
        "--minimal",
        type=strtobool,
        default=False,
        help="Asserts only the visibility data column and basic metadata matches",
    )
    parser.add_argument(
        "--repeats",
        type=int,
        default=1,
        help="Number of times data is expected to repeat in the second measurement set",
    )
    args = parser.parse_args()

    asserter = MSAsserter()
    if args.minimal:
        asserter.assert_ms_data_equal(args.filename1, args.filename2, args.repeats)
    else:
        # Perform full comparison
        asserter.assert_ms_equal(
            args.filename1,
            args.filename2,
            columns=[
                AssertProps("DATA", True, True),
                AssertProps("FLAG", True, True),
                AssertProps("WEIGHT", True, True),
                AssertProps("SIGMA", True, True),
                AssertProps("UVW", True, True),
            ],
        )
    print(f"{args.filename2} matches {args.filename1} repeated {args.repeats} times")


if __name__ == "__main__":
    main()
