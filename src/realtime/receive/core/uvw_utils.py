import datetime
import math

import numpy
from astropy.coordinates import Angle
from astropy.time import Time
from casacore.measures import measures
from casacore.quanta import quantity


def time_to_quantity(in_time: Time):
    """

    I have found that for some reason I get either a string or a byte array as returned isot format from astropy.time
    There seems to be no reason to it - but it does seem to be a function of the value. Some dates are always byte arrays
    and some are always strings - this helper function just tests what is going on tries to return a valid casacore.quanta

    :param astropy.Time in_time: astropy Time object
    :returns: a casacore.quantity of the same value

    """
    in_time.format = "isot"
    if len(in_time.value) > 1:
        time_datetime = datetime.datetime.fromisoformat(in_time.value)
    else:
        time_datetime = datetime.datetime.fromisoformat(in_time.value[0])

    out_time = quantity(time_datetime.isoformat())
    return out_time


def get_rot_x(angle: Angle) -> numpy.ndarray:
    """Return rotation matrix for rotation about the x axis

    :param astropy.coordinates.Angle angle: The angle of rotation
    :returns: The rotation matrix
    :rtype: numpy.ndarray
    """

    sin_a = math.sin(angle.radian)
    cos_a = math.cos(angle.radian)

    rot_x = numpy.array(
        [
            [1, 0, 0],
            [0, cos_a, sin_a],
            [0, -1 * sin_a, cos_a],
        ]
    )

    return rot_x


def get_rot_y(angle: Angle) -> numpy.ndarray:
    """Return rotation matrix for rotation about the Y axis

    :param astropy.coordinates.Angle angle: The angle of rotation
    :returns: The rotation matrix
    :rtype: numpy.ndarray
    """
    sin_a = math.sin(angle.radian)
    cos_a = math.cos(angle.radian)

    rot_y = numpy.array(
        [
            [cos_a, 0, -1 * sin_a],
            [0, 1, 0],
            [sin_a, 0, cos_a],
        ]
    )

    return rot_y


def get_rot_z(angle: Angle) -> numpy.ndarray:
    """Return rotation matrix for rotation about the z axis

    :param astropy.coordinates.Angle angle: The angle of rotation
    :returns: The rotation matrix
    :rtype: numpy.ndarray
    """
    sin_a = math.sin(angle.radian)
    cos_a = math.cos(angle.radian)

    rot_z = numpy.array(
        [
            [cos_a, sin_a, 0],
            [-1 * sin_a, cos_a, 0],
            [0, 0, 1],
        ]
    )

    return rot_z


def get_r(h: Angle, delta: Angle) -> numpy.ndarray:
    """

    Return the matrix that will project XYZ into UVW. This is the matrix that is typically used to get the
    transformation from a geocentric position to a UVW at the epoch of date.

    It is the combination of two rotations. The first of XY about Z by the hour angle and the second of XZ about Y

    See A.R. Thompson, J.M. Moran, and G.W. Swenson Jr.,
    Interferometry and Synthesis in Radio Astronomy (equation 4.3 in the Third Edition)

    :param double h: -- the Greenwich hour angle in radian
    :param double delta:  -- the declination in radian

    :returns: A numpy array of the matrix R
    :rtype: numpy.ndarray

    """
    sin_h = math.sin(h.radian)
    cos_h = math.cos(h.radian)
    sin_d = math.sin(delta.radian)
    cos_d = math.cos(delta.radian)

    matrix_r = numpy.array(
        [
            [sin_h, cos_h, 0],
            [-1 * sin_d * cos_h, sin_d * sin_h, cos_d],
            [cos_d * cos_h, -1 * cos_d * sin_h, sin_d],
        ]
    )

    return matrix_r


def get_uvw(
    xyposA: numpy.ndarray,
    xyposB: numpy.ndarray,
    epoch: Time,
    ra: Angle,
    decl: Angle,
    swap_baselines=False,
) -> numpy.ndarray:
    """

    Return the baseline vector for the 2 given positions in epoch of date

    :param list xyposA: position of antenna 1 (vector geocentric XYZ in m)
    :param list xyposB: position of antenna 2 (vector geocentric XYZ in m)
    :param astropy.Time epoch: Time for which to calculate the UVW
    :param astropy.Angle ra:  Right Ascension (J2000)
    :param astropy.Angle decl:  Declination (J2000)
    :param Bool swap_baselines: (xyposB -> xyposA) is assumed - if the reverse is required set this to True

    :returns: The uvw baseline at Epoch of Date
    :rtype: numpy.ndarray

    """

    if swap_baselines:
        swap = -1.0
    else:
        swap = 1.0

    hour = epoch.sidereal_time("apparent", "greenwich") - ra

    R = get_r(hour, decl)
    # Note these are geocentric positions so we need to use A-B to
    # get a baseline vector that goes from B to A
    tmp = swap * (xyposA - xyposB)
    baseline = [[tmp[0]], [tmp[1]], [tmp[2]]]

    uvw = numpy.matmul(R, baseline)

    return uvw


def get_uvw_J2000(
    xyposA: numpy.ndarray,
    xyposB: numpy.ndarray,
    refpos: numpy.ndarray,
    epoch: Time,
    ra: Angle,
    dec: Angle,
    position_frame="itrf",
    swap_baselines=False,
) -> numpy.ndarray:
    """
    Return the baseline vector for the 2 given positions at the J2000 Epoch between A and B, which is
    determined from the vector from B to A". This is consistent with the MSv2 definition.
    This ensures that any image formed by the Fourier transform of a UVW cube in this
    frame will itself be in the ICRS frame.

    Note: I believe that the ICRS and J2000 are aligned <at> J2000
    - but caveat emptor until I've sorted that out.

    Note: This assumes you have a reference position at which all the
    sidereal angles are calculated but it is more likely
    that you want a "per antenna" calculation.
    So be sure you want this before you use it.

    :param list xyposA: position of antenna 1 (vector geocentric XYZ in m)
    :param list xyposB: position of antenna 2 (vector geocentric XYZ in m)
    :param astropy.Time epoch: Time for which to calculate the UVW
    :param astropy.Angle ra:  Right Ascension (J2000)
    :param astropy.Angle dec:  Declination (J2000)
    :param str position_frame: The frame of reference for the positions ITRF is default - WGS84 is also supported
    :param Bool swap_baselines: The vector from posB to posA is assumed - if the reverse is required set this to True

    :returns: The uvw baseline at J2000
    :rtype: numpy.ndarray [u,v,w]

    """
    if swap_baselines:
        swap = -1.0
    else:
        swap = 1.0

    dm = measures()

    refant = dm.position(
        position_frame,
        quantity(refpos[0], "m"),
        quantity(refpos[1], "m"),
        quantity(refpos[2], "m"),
    )
    try:
        refant_itrf = dm.measure(refant, "itrf")

        # Reference Position
        dm.do_frame(
            refant_itrf
        )  # where are we probably should just be the observatory - although antenna based UVW are probably best

        # Reference Direction
        ra_measure = quantity(ra.rad, "rad")
        dec_measure = quantity(dec.rad, "rad")

        source = dm.direction("J2000", ra_measure, dec_measure)
        dm.do_frame(source)  # where are we looking

        # Time
        epoch_scale = epoch.scale
        epoch_val = time_to_quantity(epoch)
        epoch_measure = dm.epoch(rf=epoch_scale, v0=epoch_val)

        dm.do_frame(epoch_measure)  # what time is it

        # Baseline Epoch of Date
        # Note these are geocentric positions so we need to use A-B to
        # get a baseline vector that goes from B to A
        dx = quantity(swap * (xyposA[0] - xyposB[0]), "m")
        dy = quantity(swap * (xyposA[1] - xyposB[1]), "m")
        dz = quantity(swap * (xyposA[2] - xyposB[2]), "m")

        b = dm.baseline("itrf", dx, dy, dz)

        # Get UVW in the J2000 reference frame
        d = dm.to_uvw(b)

        uvw = numpy.array(d.get("xyz").get_value("m"))

        return uvw

    except RuntimeError as exc:
        raise RuntimeError(
            "Likely latest measures is not installed check casacore_data or WSRT Measures"
        ) from exc
