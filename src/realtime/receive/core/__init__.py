# -*- coding: utf-8 -*-
"""Module init code."""

from . import baseline_utils, config, icd, ms_asserter, msutils, time_utils
from .antenna import Antenna
from .baselines import Baselines
from .channel_range import ChannelRange
from .common import from_dict
from .scan import Scan, ScanType

# pylint: disable=import-self,protected-access

try:
    from . import version
except ImportError:
    __version__ = "testing"
else:
    __version__ = version._version

__author__ = "Stephen Ord"
__email__ = "stephen.ord@csiro.au"

__all__ = (
    "baseline_utils",
    "config",
    "from_dict",
    "icd",
    "ms_asserter",
    "msutils",
    "Antenna",
    "Baselines",
    "ChannelRange",
    "Scan",
    "ScanType",
)
