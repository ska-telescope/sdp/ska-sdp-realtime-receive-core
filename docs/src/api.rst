.. doctest-skip-all
.. _package-guide:


API documentation
=================

ICD
---

.. automodule:: realtime.receive.core.icd
   :members:


Antennas
--------

.. automodule:: realtime.receive.core.antenna
   :members:

.. automodule:: realtime.receive.core.antenna_utils
   :members:


Baselines
---------

.. automodule:: realtime.receive.core.baselines
   :members:


.. automodule:: realtime.receive.core.baseline_utils
   :members:


Scans
-----

These classes are used to describe a Scan
and its components.
Scans are the atomic pieces of an observation,
and they have important metadata
associated to them.

.. autoclass:: realtime.receive.core.scan.SpectralWindow
   :members:
.. autoclass:: realtime.receive.core.scan.Channels
   :members:
.. autoclass:: realtime.receive.core.scan.PhaseDirection
   :members:
.. autoenum:: realtime.receive.core.scan.StokesType
.. autoclass:: realtime.receive.core.scan.Polarisations
   :members:
.. autoclass:: realtime.receive.core.scan.Field
   :members:
.. autoclass:: realtime.receive.core.scan.Beam
   :members:
.. autoclass:: realtime.receive.core.scan.ScanType
   :members:
.. autoclass:: realtime.receive.core.scan.Scan
   :members:

.. automodule:: realtime.receive.core.scan_utils
   :members:


UVW Calculation and Cache
-------------------------

These methods are related to the calculation and storage of UVWs.

.. autoclass:: realtime.receive.core.uvw_engine.UVWEngine
   :members:

.. autoclass:: realtime.receive.core.katpoint_uvw_engine.KatpointUVWEngine
   :members:

Measurement Set access
----------------------

.. autoclass:: realtime.receive.core.msutils.MeasurementSet
.. autoclass:: realtime.receive.core.msutils.MSWriter

Utilities
---------

.. automodule:: realtime.receive.core.common
   :members:

.. automodule:: realtime.receive.core.time_utils
   :members:
