.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.


Core Utilities
==============

This is a small package of shared utilities used by both
the :doc:`ska-sdp-cbf-emulator:index`,
the :doc:`ska-sdp-realtime-receive-modules:index`
and the :doc:`ska-sdp-realtime-receive-processors:index` packages

.. toctree::
  :maxdepth: 2

  installation
  api
